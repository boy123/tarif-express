<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_paket extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jenis_paket_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'jenis_paket/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jenis_paket/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jenis_paket/index.html';
            $config['first_url'] = base_url() . 'jenis_paket/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Jenis_paket_model->total_rows($q);
        $jenis_paket = $this->Jenis_paket_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'jenis_paket_data' => $jenis_paket,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'jenis_paket/jenis_paket_list',
            'konten' => 'jenis_paket/jenis_paket_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Jenis_paket_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_jenis_paket' => $row->id_jenis_paket,
		'jenis_paket' => $row->jenis_paket,
	    );
            $this->load->view('jenis_paket/jenis_paket_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jenis_paket'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'jenis_paket/jenis_paket_form',
            'konten' => 'jenis_paket/jenis_paket_form',
            'button' => 'Create',
            'action' => site_url('jenis_paket/create_action'),
	    'id_jenis_paket' => set_value('id_jenis_paket'),
	    'jenis_paket' => set_value('jenis_paket'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'jenis_paket' => $this->input->post('jenis_paket',TRUE),
	    );

            $this->Jenis_paket_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('jenis_paket'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Jenis_paket_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'jenis_paket/jenis_paket_form',
                'konten' => 'jenis_paket/jenis_paket_form',
                'button' => 'Update',
                'action' => site_url('jenis_paket/update_action'),
		'id_jenis_paket' => set_value('id_jenis_paket', $row->id_jenis_paket),
		'jenis_paket' => set_value('jenis_paket', $row->jenis_paket),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jenis_paket'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_jenis_paket', TRUE));
        } else {
            $data = array(
		'jenis_paket' => $this->input->post('jenis_paket',TRUE),
	    );

            $this->Jenis_paket_model->update($this->input->post('id_jenis_paket', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('jenis_paket'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Jenis_paket_model->get_by_id($id);

        if ($row) {
            $this->Jenis_paket_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('jenis_paket'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jenis_paket'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('jenis_paket', 'jenis paket', 'trim|required');

	$this->form_validation->set_rules('id_jenis_paket', 'id_jenis_paket', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Jenis_paket.php */
/* Location: ./application/controllers/Jenis_paket.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-04-28 07:30:10 */
/* https://jualkoding.com */