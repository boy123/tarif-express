<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	
	public function index()
	{
		$this->load->view('f_home');
	}

	public function cek_tarif_proses()
	{
		$asal = $this->input->post('asal');
		$tujuan = $this->input->post('tujuan');
		$jenis_paket = $this->input->post('jenis_paket');
		$berat = $this->input->post('berat');

		$tarif = 0;
		$transit = '';
		$dt = '';

		//jika paket dokumen
		if ($jenis_paket == '0') {
			if ($berat > 0 AND $berat <= 0.5 ) {
				$sql = $this->db->get_where('tarif_dokumen', array(
					'id_pengiriman'=> $asal,
					'id_destination'=> $tujuan,
					'berat'=> 0.5
				));
				if ($sql->num_rows() == 0) {
					$this->session->set_flashdata('message', alert_biasa('Tarif tidak ditemukan','warning'));
					redirect('web','refresh');
				} else {
					$sql = $sql->row();
					$tarif = $sql->tarif;
					$transit = $sql->transit_day;
				}
				
			} elseif ($berat > 0.5 AND $berat <= 1.00 ) {
				$sql = $this->db->get_where('tarif_dokumen', array(
					'id_pengiriman'=> $asal,
					'id_destination'=> $tujuan,
					'berat'=> 1.00
				));
				if ($sql->num_rows() == 0) {
					$this->session->set_flashdata('message', alert_biasa('Tarif tidak ditemukan','warning'));
					redirect('web','refresh');
				} else {
					$sql = $sql->row();
					$tarif = $sql->tarif;
					$transit = $sql->transit_day;
				}
			} elseif ($berat > 1.00 AND $berat <= 1.50 ) {
				$sql = $this->db->get_where('tarif_dokumen', array(
					'id_pengiriman'=> $asal,
					'id_destination'=> $tujuan,
					'berat'=> 1.50
				));
				if ($sql->num_rows() == 0) {
					$this->session->set_flashdata('message', alert_biasa('Tarif tidak ditemukan','warning'));
					redirect('web','refresh');
				} else {
					$sql = $sql->row();
					$tarif = $sql->tarif;
					$transit = $sql->transit_day;
				}
			} elseif ($berat > 1.50 AND $berat <= 2.00 ) {
				$sql = $this->db->get_where('tarif_dokumen', array(
					'id_pengiriman'=> $asal,
					'id_destination'=> $tujuan,
					'berat'=> 2.00
				));
				if ($sql->num_rows() == 0) {
					$this->session->set_flashdata('message', alert_biasa('Tarif tidak ditemukan','warning'));
					redirect('web','refresh');
				} else {
					$sql = $sql->row();
					$tarif = $sql->tarif;
					$transit = $sql->transit_day;
				}
			} elseif ($berat > 2.00 AND $berat <= 2.50 ) {
				$sql = $this->db->get_where('tarif_dokumen', array(
					'id_pengiriman'=> $asal,
					'id_destination'=> $tujuan,
					'berat'=> 2.50
				));
				if ($sql->num_rows() == 0) {
					$this->session->set_flashdata('message', alert_biasa('Tarif tidak ditemukan','warning'));
					redirect('web','refresh');
				} else {
					$sql = $sql->row();
					$tarif = $sql->tarif;
					$transit = $sql->transit_day;
				}
			} else {

				$this->session->set_flashdata('message', alert_biasa('tidak ditemukan tarif yang Anda cari, silakan menghubungi kami untuk informasi lebih lanjut !','warning'));
				redirect('web','refresh');

			}


		}
		//tarif barang
		elseif ($jenis_paket > 0) {
			if ($berat == 1 ) {
				$sql = $this->db->get_where('tarif_barang', array(
					'id_pengiriman'=> $asal,
					'id_destination'=> $tujuan,
					'id_jenis_paket'=>$jenis_paket
				));
				if ($sql->num_rows() == 0) {
					$this->session->set_flashdata('message', alert_biasa('Tarif tidak ditemukan','warning'));
					redirect('web','refresh');
				} else {
					$dt = $sql;
					$sql = $sql->row();
					$tarif = $sql->first;
					$transit = $sql->transit_day;

				}
				
			} elseif ($berat > 1) {
				$sql = $this->db->get_where('tarif_barang', array(
					'id_pengiriman'=> $asal,
					'id_destination'=> $tujuan,
					'id_jenis_paket'=>$jenis_paket
				));
				if ($sql->num_rows() == 0) {
					$this->session->set_flashdata('message', alert_biasa('Tarif tidak ditemukan','warning'));
					redirect('web','refresh');
				} else {
					$dt = $sql;
					$sql = $sql->row();
					$tarif = $sql->first;
					$sisa_berat = $berat - 1;
					$tarif = $tarif + ($sql->next * $sisa_berat);
					$transit = $sql->transit_day;
				}
				
			} else {
				$this->session->set_flashdata('message', alert_biasa('Tarif tidak ditemukan','warning'));
				redirect('web','refresh');
			}
		} else {
			$this->session->set_flashdata('message', alert_biasa('ada kesalahan tidak diketahui !','warning'));
				redirect('web','refresh');
		}

		$data = array(
			'berat' => $berat,
			'asal' => get_data('asal_pengiriman','id_pengiriman',$asal,'lokasi_pengiriman'),
			'tujuan' => get_data('destination','id_destination',$tujuan,'destination'),
			'jenis_paket' => $retVal = ($jenis_paket == '0') ? 'Dokumen' : get_data('jenis_paket','id_jenis_paket',$jenis_paket,'jenis_paket') ,
			'tarif' => $tarif,
			'transit' => $transit,
			'dt'=>$dt,
			'id_jenis_paket'=>$jenis_paket
		);
		$this->load->view('f_cektarif', $data);

	}



}
