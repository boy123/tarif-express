<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tarif_barang extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tarif_barang_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tarif_barang/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tarif_barang/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tarif_barang/index.html';
            $config['first_url'] = base_url() . 'tarif_barang/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tarif_barang_model->total_rows($q);
        $tarif_barang = $this->Tarif_barang_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tarif_barang_data' => $tarif_barang,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'tarif_barang/tarif_barang_list',
            'konten' => 'tarif_barang/tarif_barang_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Tarif_barang_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_tarif_brang' => $row->id_tarif_brang,
		'id_pengiriman' => $row->id_pengiriman,
		'id_destination' => $row->id_destination,
		'id_jenis_paket' => $row->id_jenis_paket,
		'first' => $row->first,
		'next' => $row->next,
		'transit_day' => $row->transit_day,
	    );
            $this->load->view('tarif_barang/tarif_barang_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tarif_barang'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'tarif_barang/tarif_barang_form',
            'konten' => 'tarif_barang/tarif_barang_form',
            'button' => 'Create',
            'action' => site_url('tarif_barang/create_action'),
	    'id_tarif_brang' => set_value('id_tarif_brang'),
	    'id_pengiriman' => set_value('id_pengiriman'),
	    'id_destination' => set_value('id_destination'),
	    'id_jenis_paket' => set_value('id_jenis_paket'),
	    'first' => set_value('first'),
	    'next' => set_value('next'),
	    'transit_day' => set_value('transit_day'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_pengiriman' => $this->input->post('id_pengiriman',TRUE),
		'id_destination' => $this->input->post('id_destination',TRUE),
		'id_jenis_paket' => $this->input->post('id_jenis_paket',TRUE),
		'first' => $this->input->post('first',TRUE),
		'next' => $this->input->post('next',TRUE),
		'transit_day' => $this->input->post('transit_day',TRUE),
	    );

            $this->Tarif_barang_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tarif_barang'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tarif_barang_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'tarif_barang/tarif_barang_form',
                'konten' => 'tarif_barang/tarif_barang_form',
                'button' => 'Update',
                'action' => site_url('tarif_barang/update_action'),
		'id_tarif_brang' => set_value('id_tarif_brang', $row->id_tarif_brang),
		'id_pengiriman' => set_value('id_pengiriman', $row->id_pengiriman),
		'id_destination' => set_value('id_destination', $row->id_destination),
		'id_jenis_paket' => set_value('id_jenis_paket', $row->id_jenis_paket),
		'first' => set_value('first', $row->first),
		'next' => set_value('next', $row->next),
		'transit_day' => set_value('transit_day', $row->transit_day),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tarif_barang'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_tarif_brang', TRUE));
        } else {
            $data = array(
		'id_pengiriman' => $this->input->post('id_pengiriman',TRUE),
		'id_destination' => $this->input->post('id_destination',TRUE),
		'id_jenis_paket' => $this->input->post('id_jenis_paket',TRUE),
		'first' => $this->input->post('first',TRUE),
		'next' => $this->input->post('next',TRUE),
		'transit_day' => $this->input->post('transit_day',TRUE),
	    );

            $this->Tarif_barang_model->update($this->input->post('id_tarif_brang', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tarif_barang'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tarif_barang_model->get_by_id($id);

        if ($row) {
            $this->Tarif_barang_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tarif_barang'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tarif_barang'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_pengiriman', 'id pengiriman', 'trim|required');
	$this->form_validation->set_rules('id_destination', 'id destination', 'trim|required');
	$this->form_validation->set_rules('id_jenis_paket', 'id jenis paket', 'trim|required');
	$this->form_validation->set_rules('first', 'first', 'trim|required');
	$this->form_validation->set_rules('next', 'next', 'trim|required');
	$this->form_validation->set_rules('transit_day', 'transit day', 'trim|required');

	$this->form_validation->set_rules('id_tarif_brang', 'id_tarif_brang', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tarif_barang.php */
/* Location: ./application/controllers/Tarif_barang.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-04-28 07:30:17 */
/* https://jualkoding.com */