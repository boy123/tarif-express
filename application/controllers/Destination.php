<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Destination extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Destination_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'destination/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'destination/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'destination/index.html';
            $config['first_url'] = base_url() . 'destination/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Destination_model->total_rows($q);
        $destination = $this->Destination_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'destination_data' => $destination,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'destination/destination_list',
            'konten' => 'destination/destination_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Destination_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_destination' => $row->id_destination,
		'destination' => $row->destination,
	    );
            $this->load->view('destination/destination_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('destination'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'destination/destination_form',
            'konten' => 'destination/destination_form',
            'button' => 'Create',
            'action' => site_url('destination/create_action'),
	    'id_destination' => set_value('id_destination'),
	    'destination' => set_value('destination'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'destination' => $this->input->post('destination',TRUE),
	    );

            $this->Destination_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('destination'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Destination_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'destination/destination_form',
                'konten' => 'destination/destination_form',
                'button' => 'Update',
                'action' => site_url('destination/update_action'),
		'id_destination' => set_value('id_destination', $row->id_destination),
		'destination' => set_value('destination', $row->destination),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('destination'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_destination', TRUE));
        } else {
            $data = array(
		'destination' => $this->input->post('destination',TRUE),
	    );

            $this->Destination_model->update($this->input->post('id_destination', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('destination'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Destination_model->get_by_id($id);

        if ($row) {
            $this->Destination_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('destination'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('destination'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('destination', 'destination', 'trim|required');

	$this->form_validation->set_rules('id_destination', 'id_destination', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Destination.php */
/* Location: ./application/controllers/Destination.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-04-28 07:30:06 */
/* https://jualkoding.com */