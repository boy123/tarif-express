<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Asal_pengiriman extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Asal_pengiriman_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'asal_pengiriman/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'asal_pengiriman/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'asal_pengiriman/index.html';
            $config['first_url'] = base_url() . 'asal_pengiriman/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Asal_pengiriman_model->total_rows($q);
        $asal_pengiriman = $this->Asal_pengiriman_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'asal_pengiriman_data' => $asal_pengiriman,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'asal_pengiriman/asal_pengiriman_list',
            'konten' => 'asal_pengiriman/asal_pengiriman_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Asal_pengiriman_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_pengiriman' => $row->id_pengiriman,
		'lokasi_pengiriman' => $row->lokasi_pengiriman,
	    );
            $this->load->view('asal_pengiriman/asal_pengiriman_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('asal_pengiriman'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'asal_pengiriman/asal_pengiriman_form',
            'konten' => 'asal_pengiriman/asal_pengiriman_form',
            'button' => 'Create',
            'action' => site_url('asal_pengiriman/create_action'),
	    'id_pengiriman' => set_value('id_pengiriman'),
	    'lokasi_pengiriman' => set_value('lokasi_pengiriman'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'lokasi_pengiriman' => $this->input->post('lokasi_pengiriman',TRUE),
	    );

            $this->Asal_pengiriman_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('asal_pengiriman'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Asal_pengiriman_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'asal_pengiriman/asal_pengiriman_form',
                'konten' => 'asal_pengiriman/asal_pengiriman_form',
                'button' => 'Update',
                'action' => site_url('asal_pengiriman/update_action'),
		'id_pengiriman' => set_value('id_pengiriman', $row->id_pengiriman),
		'lokasi_pengiriman' => set_value('lokasi_pengiriman', $row->lokasi_pengiriman),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('asal_pengiriman'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pengiriman', TRUE));
        } else {
            $data = array(
		'lokasi_pengiriman' => $this->input->post('lokasi_pengiriman',TRUE),
	    );

            $this->Asal_pengiriman_model->update($this->input->post('id_pengiriman', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('asal_pengiriman'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Asal_pengiriman_model->get_by_id($id);

        if ($row) {
            $this->Asal_pengiriman_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('asal_pengiriman'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('asal_pengiriman'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('lokasi_pengiriman', 'lokasi pengiriman', 'trim|required');

	$this->form_validation->set_rules('id_pengiriman', 'id_pengiriman', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Asal_pengiriman.php */
/* Location: ./application/controllers/Asal_pengiriman.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-04-28 07:30:02 */
/* https://jualkoding.com */