<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tarif_dokumen extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tarif_dokumen_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tarif_dokumen/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tarif_dokumen/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tarif_dokumen/index.html';
            $config['first_url'] = base_url() . 'tarif_dokumen/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tarif_dokumen_model->total_rows($q);
        $tarif_dokumen = $this->Tarif_dokumen_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tarif_dokumen_data' => $tarif_dokumen,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'tarif_dokumen/tarif_dokumen_list',
            'konten' => 'tarif_dokumen/tarif_dokumen_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Tarif_dokumen_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_tarif_dokumen' => $row->id_tarif_dokumen,
		'id_pengiriman' => $row->id_pengiriman,
		'id_destination' => $row->id_destination,
		'berat' => $row->berat,
		'tarif' => $row->tarif,
		'transit_day' => $row->transit_day,
	    );
            $this->load->view('tarif_dokumen/tarif_dokumen_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tarif_dokumen'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'tarif_dokumen/tarif_dokumen_form',
            'konten' => 'tarif_dokumen/tarif_dokumen_form',
            'button' => 'Create',
            'action' => site_url('tarif_dokumen/create_action'),
	    'id_tarif_dokumen' => set_value('id_tarif_dokumen'),
	    'id_pengiriman' => set_value('id_pengiriman'),
	    'id_destination' => set_value('id_destination'),
	    'berat' => set_value('berat'),
	    'tarif' => set_value('tarif'),
	    'transit_day' => set_value('transit_day'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_pengiriman' => $this->input->post('id_pengiriman',TRUE),
		'id_destination' => $this->input->post('id_destination',TRUE),
		'berat' => $this->input->post('berat',TRUE),
		'tarif' => $this->input->post('tarif',TRUE),
		'transit_day' => $this->input->post('transit_day',TRUE),
	    );

            $this->Tarif_dokumen_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tarif_dokumen'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tarif_dokumen_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'tarif_dokumen/tarif_dokumen_form',
                'konten' => 'tarif_dokumen/tarif_dokumen_form',
                'button' => 'Update',
                'action' => site_url('tarif_dokumen/update_action'),
		'id_tarif_dokumen' => set_value('id_tarif_dokumen', $row->id_tarif_dokumen),
		'id_pengiriman' => set_value('id_pengiriman', $row->id_pengiriman),
		'id_destination' => set_value('id_destination', $row->id_destination),
		'berat' => set_value('berat', $row->berat),
		'tarif' => set_value('tarif', $row->tarif),
		'transit_day' => set_value('transit_day', $row->transit_day),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tarif_dokumen'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_tarif_dokumen', TRUE));
        } else {
            $data = array(
		'id_pengiriman' => $this->input->post('id_pengiriman',TRUE),
		'id_destination' => $this->input->post('id_destination',TRUE),
		'berat' => $this->input->post('berat',TRUE),
		'tarif' => $this->input->post('tarif',TRUE),
		'transit_day' => $this->input->post('transit_day',TRUE),
	    );

            $this->Tarif_dokumen_model->update($this->input->post('id_tarif_dokumen', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tarif_dokumen'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tarif_dokumen_model->get_by_id($id);

        if ($row) {
            $this->Tarif_dokumen_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tarif_dokumen'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tarif_dokumen'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_pengiriman', 'id pengiriman', 'trim|required');
	$this->form_validation->set_rules('id_destination', 'id destination', 'trim|required');
	$this->form_validation->set_rules('berat', 'berat', 'trim|required');
	$this->form_validation->set_rules('tarif', 'tarif', 'trim|required');
	$this->form_validation->set_rules('transit_day', 'transit day', 'trim|required');

	$this->form_validation->set_rules('id_tarif_dokumen', 'id_tarif_dokumen', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tarif_dokumen.php */
/* Location: ./application/controllers/Tarif_dokumen.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-04-28 07:30:20 */
/* https://jualkoding.com */