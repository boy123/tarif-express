<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Cek Tarif Timexpress</title>
  <base href="<?php echo base_url() ?>">
  <meta content="" name="Cek tarif timexpress">
  <meta content="" name="Cek Tarif Timexpress">

  <!-- Favicons -->
  <link href="front/img/favicon.png" rel="icon">
  <link href="front/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="front/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="front/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="front/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="front/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="front/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="front/vendor/owl.carousel/front/owl.carousel.min.css" rel="stylesheet">
  <link href="front/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="front/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Bootslander - v2.1.0
  * Template URL: https://bootstrapmade.com/bootslander-free-bootstrap-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href=""><span>Cek Tarif Timexpress</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="front/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>
      <?php $this->load->view('f_menu'); ?>
      <!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">

    <div class="container">
      <div class="row">
        <div class="col-lg-7 pt-5 pt-lg-0 order-2 order-lg-1 d-flex align-items-center">
          <div data-aos="zoom-out">
            <h1>Cek Tarif kami hanya melalui genggaman <!-- <span>Bootstlander</span> --> </h1>
            <!-- <h2>We are team of talanted designers making websites with Bootstrap</h2> -->
            <div class="text-center text-lg-left">
              <a href="#" data-toggle="modal" data-target="#myModal" class="btn-get-started scrollto">Cek Tarif Sekarang</a>
            </div>
          </div>
        </div>
        <div class="col-lg-5 order-1 order-lg-2 hero-img" data-aos="zoom-out" data-aos-delay="300">
          <img src="front/img/hero-img.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>

  </section><!-- End Hero -->

  <main id="main">


    <!-- The Modal -->
    <div class="modal" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header alert-warning">
            <h4 class="modal-title">Cek Tarif Anda</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <div class="modal-body">
            
            <form action="web/cek_tarif_proses" method="POST">
              <div class="form-group">
                <label>Asal</label>
                <select name="asal" class="form-control" required="">
                  <option value="">Daerah Asal</option>
                  <?php foreach ($this->db->get('asal_pengiriman')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_pengiriman ?>"><?php echo $rw->lokasi_pengiriman ?></option>
                  <?php endforeach ?>
                </select>
              </div>

              <div class="form-group">
                <label>Tujuan</label>
                <select name="tujuan" class="form-control" required="">
                  <option value="">Pilih Tujuan</option>
                  <?php foreach ($this->db->get('destination')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_destination ?>"><?php echo $rw->destination ?></option>
                  <?php endforeach ?>
                </select>
              </div>

              <div class="form-group">
                <label>Jenis Paket</label>
                <select name="jenis_paket" class="form-control" required="">
                  <option value="">Pilih Jenis Paket</option>
                  <option value="0">Dokumen</option>
                  <?php foreach ($this->db->get('jenis_paket')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_jenis_paket ?>"><?php echo $rw->jenis_paket ?></option>
                  <?php endforeach ?>
                </select>
              </div>

              <div class="form-group">
                <label>Berat/Kg</label>
                <input type="text" name="berat" placeholder="Perkiraan Berat" class="form-control" required="">
              </div>

               <div class="form-group">
                <button type="submit" class="btn btn-block btn-warning">Dapatkan Tarif</button>
              </div>

            </form>

          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            
          </div>
          
        </div>
      </div>
    </div>

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container-fluid">

        <div class="row">
          <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch" data-aos="fade-right">
            
          </div>

          <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5" data-aos="fade-left">
            <h3>Mengapa Kami ?</h3>
            

            <div class="icon-box" data-aos="zoom-in" data-aos-delay="100">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">Ekspor Import</a></h4>
              <p class="description">Melayani Ekspor Import dengan tim yang profesional</p>
            </div>

            <div class="icon-box" data-aos="zoom-in" data-aos-delay="200">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Door To Door</a></h4>
              <p class="description">Melakukan pengiriman langsung ke depan pintu rumah anda</p>
            </div>

            <div class="icon-box" data-aos="zoom-in" data-aos-delay="300">
              <div class="icon"><i class="bx bx-atom"></i></div>
              <h4 class="title"><a href="">Jangkauan Luas</a></h4>
              <p class="description">Menjangkau pengiriman ke negara manapun</p>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

   

    

    
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span> 2020</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bootslander-free-bootstrap-landing-page-template/ -->
        <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="front/vendor/jquery/jquery.min.js"></script>
  <script src="front/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="front/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="front/vendor/php-email-form/validate.js"></script>
  <script src="front/vendor/venobox/venobox.min.js"></script>
  <script src="front/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="front/vendor/counterup/counterup.min.js"></script>
  <script src="front/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="front/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="front/js/main.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript"><?php echo $this->session->userdata('message') ?></script>

</body>

</html>