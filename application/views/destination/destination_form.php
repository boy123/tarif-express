
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Destination <?php echo form_error('destination') ?></label>
            <input type="text" class="form-control" name="destination" id="destination" placeholder="Destination" value="<?php echo $destination; ?>" />
        </div>
	    <input type="hidden" name="id_destination" value="<?php echo $id_destination; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('destination') ?>" class="btn btn-default">Cancel</a>
	</form>
   