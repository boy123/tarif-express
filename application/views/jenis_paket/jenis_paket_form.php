
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Jenis Paket <?php echo form_error('jenis_paket') ?></label>
            <input type="text" class="form-control" name="jenis_paket" id="jenis_paket" placeholder="Jenis Paket" value="<?php echo $jenis_paket; ?>" />
        </div>
	    <input type="hidden" name="id_jenis_paket" value="<?php echo $id_jenis_paket; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('jenis_paket') ?>" class="btn btn-default">Cancel</a>
	</form>
   