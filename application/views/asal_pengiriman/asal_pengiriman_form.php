
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Lokasi Pengiriman <?php echo form_error('lokasi_pengiriman') ?></label>
            <input type="text" class="form-control" name="lokasi_pengiriman" id="lokasi_pengiriman" placeholder="Lokasi Pengiriman" value="<?php echo $lokasi_pengiriman; ?>" />
        </div>
	    <input type="hidden" name="id_pengiriman" value="<?php echo $id_pengiriman; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('asal_pengiriman') ?>" class="btn btn-default">Cancel</a>
	</form>
   