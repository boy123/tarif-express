<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Cek Tarif Timexpress</title>
  <base href="<?php echo base_url() ?>">
  <meta content="" name="Cek Tarif Timexpress">
  <meta content="" name="Cek Tarif Timexpress">

  <!-- Favicons -->
  <link href="front/img/favicon.png" rel="icon">
  <link href="front/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="front/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="front/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="front/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="front/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="front/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="front/vendor/owl.carousel/front/owl.carousel.min.css" rel="stylesheet">
  <link href="front/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="front/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Bootslander - v2.1.0
  * Template URL: https://bootstrapmade.com/bootslander-free-bootstrap-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href=""><span>Cek Tarif Timexpress</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="front/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>
      <?php $this->load->view('f_menu'); ?>
      <!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">

    <div class="container">
      <div class="row">
       
      </div>
    </div>

    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>

  </section><!-- End Hero -->

  <main id="main">

    
   

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Tarif yang anda cari</h2>
          <!-- <p>Tarif sudah termasuk biaya pengiriman dari daerah,</p> -->
        </div>

        <div class="row" data-aos="fade-left" >
          <div class="col-lg-3 col-md-3">
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="box" data-aos="zoom-in" data-aos-delay="100">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group" style="text-align: left;">
                    <label>Asal</label>
                    <p><b><?php echo $asal ?>, Indonesia</b></p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group" style="text-align: left;">
                    <label>Tujuan</label>
                    <p><b><?php echo $tujuan ?></b></p>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group" style="text-align: left;">
                    <label>Berat</label>
                    <p><b><?php 
                    echo str_replace('.', ',', $berat) ?> Kg</b></p>
                  </div>
                </div>
              </div>
              
            </div>
          </div>

          

        </div>
        <br>
        <div class="row">
          <div class="col-lg-12 col-md-12 mt-4 mt-md-0">
            <div class="box featured" data-aos="zoom-in" data-aos-delay="200">
              <h3><?php echo $jenis_paket ?></h3>
              <?php 
              if ($id_jenis_paket != 0) {
                foreach ($dt->result() as $rw) {
                  if ($berat == 1) {
                    $tarif = $rw->first;
                    $transit = $rw->transit_day;
                  } else {
                    $tarif = $rw->first;
                    $sisa_berat = $berat - 1;
                    $tarif = $tarif + ($rw->next * $sisa_berat);
                    $transit = $rw->transit_day;
                  }
                ?>
                <h2><sup>Rp </sup> <?php
                $tarif_x = number_format($tarif);
                $tarif = str_replace(',', '.',$tarif_x);
                 echo $tarif ?><span> / <?php echo $transit ?></span></h2>
                 <p></p>
                <?php
                }
              } else {
               ?>
                
                <h2><sup>Rp </sup> <?php
                $tarif_x = number_format($tarif);
                $tarif = str_replace(',', '.',$tarif_x);
                 echo $tarif ?><span> / <?php echo $transit ?></span></h2>
              <?php } ?>
            </div>
          </div>
        </div>


      </div>
    </section><!-- End Pricing Section -->

    
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span> 2020</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bootslander-free-bootstrap-landing-page-template/ -->
        <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="front/vendor/jquery/jquery.min.js"></script>
  <script src="front/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="front/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="front/vendor/php-email-form/validate.js"></script>
  <script src="front/vendor/venobox/venobox.min.js"></script>
  <script src="front/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="front/vendor/counterup/counterup.min.js"></script>
  <script src="front/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="front/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="front/js/main.js"></script>

</body>

</html>