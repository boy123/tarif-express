
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('tarif_barang/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('tarif_barang/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('tarif_barang'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Pengiriman</th>
		<th>Destination</th>
		<th>Jenis Paket</th>
		<th>First</th>
		<th>Next</th>
		<th>Transit Day</th>
		<th>Action</th>
            </tr><?php
            foreach ($tarif_barang_data as $tarif_barang)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo get_data('asal_pengiriman','id_pengiriman',$tarif_barang->id_pengiriman,'lokasi_pengiriman');  ?></td>
            <td><?php echo get_data('destination','id_destination',$tarif_barang->id_destination,'destination') ?></td>
			<td><?php echo get_data('jenis_paket','id_jenis_paket',$tarif_barang->id_jenis_paket,'jenis_paket') ?></td>
			<td><?php echo $tarif_barang->first ?></td>
			<td><?php echo $tarif_barang->next ?></td>
			<td><?php echo $tarif_barang->transit_day ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('tarif_barang/update/'.$tarif_barang->id_tarif_brang),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('tarif_barang/delete/'.$tarif_barang->id_tarif_brang),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    