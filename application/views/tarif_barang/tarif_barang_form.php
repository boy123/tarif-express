
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Pengiriman <?php echo form_error('id_pengiriman') ?></label>
            <!-- <input type="text" class="form-control" name="id_pengiriman" id="id_pengiriman" placeholder="Id Pengiriman" value="<?php echo $id_pengiriman; ?>" /> -->
            <select name="id_pengiriman" class="form-control select2">
                <option value="<?php echo $id_pengiriman ?>"><?php echo get_data('asal_pengiriman','id_pengiriman',$id_pengiriman,'lokasi_pengiriman') ?></option>
                <?php foreach ($this->db->get('asal_pengiriman')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_pengiriman ?>"><?php echo $rw->lokasi_pengiriman ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Destination <?php echo form_error('id_destination') ?></label>
            <!-- <input type="text" class="form-control" name="id_destination" id="id_destination" placeholder="Id Destination" value="<?php echo $id_destination; ?>" /> -->
            <select name="id_destination" class="form-control select2">
                <option value="<?php echo $id_destination ?>"><?php echo get_data('destination','id_destination',$id_destination,'destination') ?></option>
                <?php foreach ($this->db->get('destination')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_destination ?>"><?php echo $rw->destination ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Jenis Paket <?php echo form_error('id_jenis_paket') ?></label>
            <!-- <input type="text" class="form-control" name="id_jenis_paket" id="id_jenis_paket" placeholder="Id Jenis Paket" value="<?php echo $id_jenis_paket; ?>" /> -->
            <select name="id_jenis_paket" class="form-control select2">
                <option value="<?php echo $id_jenis_paket ?>"><?php echo get_data('jenis_paket','id_jenis_paket',$id_jenis_paket,'jenis_paket') ?></option>
                <?php foreach ($this->db->get('jenis_paket')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_jenis_paket ?>"><?php echo $rw->jenis_paket ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">First Kg<?php echo form_error('first') ?></label>
            <input type="text" class="form-control" name="first" id="first" placeholder="First" value="<?php echo $first; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Next Kg<?php echo form_error('next') ?></label>
            <input type="text" class="form-control" name="next" id="next" placeholder="Next" value="<?php echo $next; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Transit Day <?php echo form_error('transit_day') ?></label>
            <input type="text" class="form-control" name="transit_day" id="transit_day" placeholder="Transit Day" value="<?php echo $transit_day; ?>" />
        </div>
	    <input type="hidden" name="id_tarif_brang" value="<?php echo $id_tarif_brang; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('tarif_barang') ?>" class="btn btn-default">Cancel</a>
	</form>
   