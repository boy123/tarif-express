
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Pengiriman <?php echo form_error('id_pengiriman') ?></label>
            <!-- <input type="text" class="form-control" name="id_pengiriman" id="id_pengiriman" placeholder="Id Pengiriman" value="<?php echo $id_pengiriman; ?>" /> -->
            <select name="id_pengiriman" class="form-control select2">
                <option value="<?php echo $id_pengiriman ?>"><?php echo get_data('asal_pengiriman','id_pengiriman',$id_pengiriman,'lokasi_pengiriman') ?></option>
                <?php foreach ($this->db->get('asal_pengiriman')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_pengiriman ?>"><?php echo $rw->lokasi_pengiriman ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Destination <?php echo form_error('id_destination') ?></label>
            <!-- <input type="text" class="form-control" name="id_destination" id="id_destination" placeholder="Id Destination" value="<?php echo $id_destination; ?>" /> -->
            <select name="id_destination" class="form-control select2">
                <option value="<?php echo $id_destination ?>"><?php echo get_data('destination','id_destination',$id_destination,'destination') ?></option>
                <?php foreach ($this->db->get('destination')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_destination ?>"><?php echo $rw->destination ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="float">Berat <?php echo form_error('berat') ?></label>
            <input type="text" class="form-control" name="berat" id="berat" placeholder="Berat" value="<?php echo $berat; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Tarif <?php echo form_error('tarif') ?></label>
            <input type="text" class="form-control" name="tarif" id="tarif" placeholder="Tarif" value="<?php echo $tarif; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Transit Day <?php echo form_error('transit_day') ?></label>
            <input type="text" class="form-control" name="transit_day" id="transit_day" placeholder="Transit Day" value="<?php echo $transit_day; ?>" />
        </div>
	    <input type="hidden" name="id_tarif_dokumen" value="<?php echo $id_tarif_dokumen; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('tarif_dokumen') ?>" class="btn btn-default">Cancel</a>
	</form>
   